package xrequestidmiddleware

import (
	"context"
	"github.com/google/uuid"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func RequestIDInterceptor() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		// Проверяем наличие X-Request-Id в метаданных
		md, ok := runtime.ServerMetadataFromContext(ctx)
		if !ok {
			return nil, status.Errorf(codes.Internal, "metadata is not provided")
		}

		if len(md.HeaderMD.Get("x-request-id")) == 0 {
			// X-Request-Id отсутствует, создаем новый и добавляем его в метаданные
			requestID := uuid.New().String()
			ctx = context.WithValue(ctx, "x-request-id", requestID)
			//md.HeaderMD.Set("x-request-id", requestID)
		}
		// Продолжаем обработку запроса
		return handler(ctx, req)
	}
}
